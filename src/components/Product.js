import React from 'react'
import {Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Product ({productProp}) {

	return (

			<Card id="cardprod" className =" bg-dark text-light justify-content-md-center">
				<Card.Body>
					<Card.Img variant="top" src ={productProp.fileInput}/>
					<Card.Title>{productProp.name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{productProp.description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>{productProp.price}</Card.Text>
				</Card.Body>
			</Card>
			)
}